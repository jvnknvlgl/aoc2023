import math
from itertools import count, pairwise

import numpy as np


def parse(lines: list) -> list:
    return np.rot90(list(map(list, lines))).tolist()


def solve(dish: list) -> list:
    for i, line in enumerate(dish):
        for _ in range(math.factorial(len(line))):
            changed = False

            for j, (a, b) in enumerate(pairwise(line)):
                if a == "." and b == "O":
                    dish[i][j], dish[i][j + 1] = dish[i][j + 1], dish[i][j]

                    changed = True

            if not changed:
                break

    return dish


def calc_1(dish: list) -> int:
    res = 0

    dish = solve(dish)

    for i, line in enumerate(np.rot90(dish, -1).tolist()):
        res += (len(line) - i) * line.count("O")

    return res


def calc_2(dish: list) -> int:
    res = 0
    hists = [tuple([tuple(line) for line in dish])]

    for i in count(1):
        for _ in range(4):
            dish = np.rot90(solve(dish), -1).tolist()

        hashable = tuple(map(tuple, dish))

        if hashable in hists:
            break

        hists.append(hashable)

    a = hists.index(hashable)
    dish = list(map(list, hists[a + (int(1e9) - a) % (i - a)]))

    for i, line in enumerate(np.rot90(dish, -1).tolist()):
        res += (len(line) - i) * line.count("O")

    return res


def run(lines: list) -> tuple:
    dish = parse(lines)

    res_1 = calc_1(dish)
    res_2 = calc_2(dish)

    assert res_1 == 106990
    assert res_2 == 100531

    return res_1, res_2
