import re
from functools import reduce

import numpy as np


def parse(lines: list) -> list:
    return [
        "".join(line)
        for line in np.pad(
            [list(line) for line in lines], 1, constant_values="."
        ).tolist()
    ]


def calc(scheme: list) -> tuple:
    res_1 = 0
    res_2 = 0

    gears: dict[tuple, list] = {}

    for i in range(1, len(scheme) - 1):
        for m in re.finditer(r"\d+", scheme[i]):
            bgn, end = m.span()

            for j in range(i - 1, i + 2):
                for k in range(bgn - 1, end + 1):
                    if not scheme[j][k].isdigit() and scheme[j][k] != ".":
                        res_1 += int(m.group())

                    if scheme[j][k] == "*":
                        if (j, k) not in gears.keys():
                            gears[(j, k)] = [int(m.group())]
                        else:
                            gears[(j, k)].append(int(m.group()))

    for key in gears.keys():
        if len(gears[key]) == 2:
            res_2 += reduce(lambda x, y: x * y, gears[key])

    return res_1, res_2


def run(lines: list) -> tuple:
    res_1, res_2 = calc(parse(lines))

    assert res_1 == 556057
    assert res_2 == 82824352

    return res_1, res_2
