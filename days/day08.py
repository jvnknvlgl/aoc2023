import math
import re


def parse(lines: list) -> tuple:
    instrs = []
    mapper = {}

    for instr in list(lines[0]):
        instrs.append(0 if instr == "L" else 1)

    for i in range(2, len(lines)):
        line = re.split(r"\W+", lines[i])
        mapper[line[0]] = line[1:]

    return instrs, mapper


def calc_1(instrs: list, mapper: dict) -> int:
    res = 0
    node = "AAA"

    while node != "ZZZ":
        node = mapper[node][instrs[res % len(instrs)]]
        res += 1

    return res


def calc_2(instrs: list, mapper: dict) -> int:
    res = 0
    nodes = [node for node in mapper.keys() if node[-1] == "A"]
    steps = [0] * len(nodes)

    while 0 in steps:
        i = res % len(instrs)

        ready = [node[-1] == "Z" for node in nodes]
        nodes = [mapper[node][instrs[i]] for node in nodes]

        for i in range(len(ready)):
            if ready[i] and steps[i] == 0:
                steps[i] = res

        res += 1

    return math.lcm(*steps)


def run(lines: list) -> tuple:
    instrs, mapper = parse(lines)

    res_1 = calc_1(instrs, mapper)
    res_2 = calc_2(instrs, mapper)

    assert res_1 == 13939
    assert res_2 == 8906539031197

    return res_1, res_2
