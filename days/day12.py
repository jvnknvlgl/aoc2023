from functools import cache


def parse(line: str) -> tuple:
    condis, counts = line.split()

    return condis, tuple(map(int, counts.split(",")))


@cache
def calc(condis: str, counts: tuple) -> int:
    res = 0

    for i in range(len(condis) - sum(counts[1:]) + len(counts) - counts[0]):
        prefix = "." * i + "#" * counts[0] + "."

        if (
            False
            if len(prefix) > len(condis)
            and any(char == "#" for char in prefix[len(condis) :])
            else all(a == b or b == "?" for a, b in zip(prefix, condis))
        ):
            if len(counts) == 1:
                if all(char != "#" for char in condis[len(prefix) :]):
                    res += 1
            else:
                res += calc(condis[len(prefix) :], counts[1:])

    return res


def run(lines: list) -> tuple:
    res_1 = 0
    res_2 = 0

    for line in lines:
        condis, counts = parse(line)

        res_1 += calc(condis, counts)
        res_2 += calc("?".join([condis] * 5), counts * 5)

    assert res_1 == 7017
    assert res_2 == 527570479489

    return res_1, res_2
