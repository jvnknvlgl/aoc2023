from itertools import accumulate, pairwise


def parse_1(lines: list) -> list:
    return [
        ((int(dist), 0), (0, int(dist)), (-int(dist), 0), (0, -int(dist)))[
            "RDLU".index(dir)
        ]
        for dir, dist, _ in [line.split() for line in lines]
    ]


def parse_2(lines: list) -> list:
    return [
        (
            (int(color[2:7], 16), 0),
            (0, int(color[2:7], 16)),
            (-int(color[2:7], 16), 0),
            (0, -int(color[2:7], 16)),
        )[int(color[7])]
        for _, _, color in [line.split() for line in lines]
    ]


def calc(steps: list) -> int:
    path = list(zip(*map(accumulate, zip(*steps))))

    return (
        sum(
            xa * yb - ya * xb + abs(xb - xa + yb - ya)
            for (xa, ya), (xb, yb) in pairwise(path + path[:1])
        )
        // 2
        + 1
    )


def run(lines: list) -> tuple:
    res_1 = calc(parse_1(lines))
    res_2 = calc(parse_2(lines))

    assert res_1 == 34329
    assert res_2 == 42617947302920

    return res_1, res_2
