from matplotlib.path import Path

dirs = {
    ("|", 1, 0): (1, 0),
    ("|", -1, 0): (-1, 0),
    ("-", 0, 1): (0, 1),
    ("-", 0, -1): (0, -1),
    ("L", 1, 0): (0, 1),
    ("L", 0, -1): (-1, 0),
    ("J", 1, 0): (0, -1),
    ("J", 0, 1): (-1, 0),
    ("7", -1, 0): (0, -1),
    ("7", 0, 1): (1, 0),
    ("F", -1, 0): (0, 1),
    ("F", 0, -1): (1, 0),
}


def parse(lines: list) -> tuple:
    maze = [list(line) for line in lines]

    for i, line in enumerate(maze):
        for j, _ in enumerate(line):
            if maze[i][j] == "S":
                return maze, i, j

    return maze, 0, 0


def calc(maze: list, i: int, j: int) -> tuple:
    path = [(i, j)]
    pnts = []

    i_next, j_next = 0, 0

    if maze[i - 1][j] in ["|", "L"] and i > 0:
        i_next = -1
    elif maze[i + 1][j] in ["|", "J"]:
        i_next = 1
    elif maze[i][j - 1] in ["-", "F"] and j > 0:
        j_next = -1
    else:
        j_next = 1

    while maze[i + i_next][j + j_next] != "S":
        i += i_next
        j += j_next

        path.append((i, j))

        i_next, j_next = dirs[(maze[i][j], i_next, j_next)]

    s = set(path)
    for i, line in enumerate(maze):
        for j, _ in enumerate(line):
            if (i, j) not in s:
                pnts.append((i, j))

    res_1 = int((len(path) + 1) / 2)
    res_2 = sum(Path(path).contains_points(pnts))

    return res_1, res_2


def run(lines: list) -> tuple:
    maze, i, j = parse(lines)
    res_1, res_2 = calc(maze, i, j)

    assert res_1 == 7030
    assert res_2 == 285

    return calc(maze, i, j)
