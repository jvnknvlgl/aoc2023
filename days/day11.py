from itertools import combinations

import numpy as np


def parse(lines: list) -> tuple:
    uni = np.array(
        [
            list(map(int, list(line.replace(".", "0").replace("#", "1"))))
            for line in lines
        ]
    )

    empty = [np.where(~uni.any(axis=i))[0].tolist() for i in [0, 1]]

    gal = []
    for i, line in enumerate(uni):
        for j, _ in enumerate(line):
            if uni[i, j] == 1:
                gal.append((i, j))

    return gal, empty


def calc(gal: list, empty: list, sizes: list) -> tuple:
    res = [0, 0]

    for a, b in combinations(gal, 2):
        i_min, i_max = min(a[0], b[0]), max(a[0], b[0])
        j_min, j_max = min(a[1], b[1]), max(a[1], b[1])

        for k, size in enumerate(sizes):
            res[k] += (i_max - i_min + j_max - j_min) + (
                size
                * (
                    len([y for y in empty[0] if y > j_min and y < j_max])
                    + len([x for x in empty[1] if x > i_min and x < i_max])
                )
            )

    return res[0], res[1]


def run(lines: list) -> tuple:
    gal, empty = parse(lines)

    res_1, res_2 = calc(gal, empty, [1, 999999])

    assert res_1 == 9545480
    assert res_2 == 406725732046

    return res_1, res_2
