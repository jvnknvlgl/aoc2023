def run(lines: list) -> tuple:
    res_1 = 0
    res_2 = 0

    for _ in lines:
        continue

    assert res_1 == 0
    assert res_2 == 0

    return res_1, res_2
