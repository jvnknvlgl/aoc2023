def parse(lines: list) -> tuple:
    seeds = [int(c) for c in lines[0].split(": ")[1].split()]

    mappers = []
    lines_iter = iter(lines)

    for line in lines_iter:
        if "map" in line:
            mapper = []
            line = next(lines_iter, "")

            while line:
                mapper.append([int(c) for c in line.split()])
                line = next(lines_iter, "")

            mappers.append(mapper)

    return seeds, mappers


def calc_1(seeds: list, mappers: list) -> int:
    res = []

    for seed in seeds:
        for mapper in mappers:
            for val in mapper:
                if seed in range(val[1], val[1] + val[2]):
                    seed = seed - val[1] + val[0]

                    break

        res.append(seed)

    return min(res)


def calc_2(seeds: list, mappers: list) -> int:
    res = []

    for i in range(0, len(seeds), 2):
        seed_curr = seeds[i]

        while seed_curr < seeds[i] + seeds[i + 1]:
            seed, seed_curr = seed_curr, 0

            for mapper in mappers:
                for val in mapper:
                    if seed in range(val[1], val[1] + val[2]):
                        if seed_curr == 0:
                            seed_curr = val[1] + val[2]

                        seed = seed - val[1] + val[0]

                        break

            res.append(seed)

    return min(res)


def run(lines: list) -> tuple:
    seeds, mappers = parse(lines)

    res_1 = calc_1(seeds, mappers)
    res_2 = calc_2(seeds, mappers)

    assert res_1 == 309796150
    assert res_2 == 50716416

    return res_1, res_2
