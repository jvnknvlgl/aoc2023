bag = {
    "red": 12,
    "green": 13,
    "blue": 14,
}


def run(lines: list) -> tuple:
    res_1 = 0
    res_2 = 0

    for line in lines:
        game, hands = line.replace("Game ", "").split(":")

        err = False
        mins: dict[str, int] = {}
        res = 1

        for hand in hands.split(";"):
            for cube in hand.split(","):
                count = int(cube.split()[0])
                color = cube.split()[1]

                err = count > bag[color] or err

                try:
                    mins[color] = count if mins[color] < count else mins[color]
                except KeyError:
                    mins[color] = count

        if not err:
            res_1 += int(game)

        for key in mins.keys():
            res *= mins[key]

        res_2 += res

    assert res_1 == 2449
    assert res_2 == 63981

    return res_1, res_2
