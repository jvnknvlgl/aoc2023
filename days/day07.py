def parse(lines: list, names: list, ass: int) -> list:
    cards = []

    for line in lines:
        card = list(line.split()[0])

        res = [card.count(value) for value in names]

        if ass == 2:
            res[res.index(max(res[0 : (len(res) - 1)]))] += res[names.index("J")]
            res[names.index("J")] = 0

        if 5 in res:
            rank = 0
        elif 4 in res:
            rank = 1
        elif 3 in res and 2 in res:
            rank = 2
        elif 3 in res:
            rank = 3
        elif 2 in [res.count(n) for n in res]:
            rank = 4
        elif 2 in res:
            rank = 5
        else:
            rank = 6

        cards.append((card, int(line.split()[1]), rank))

    return cards


def calc(cards: list, names: list) -> int:
    res = 0

    cards = sorted(
        cards,
        key=lambda x: [x[2]] + [names.index(a) for a in x[0]],
        reverse=True,
    )

    for i, card in enumerate(cards):
        res += (i + 1) * card[1]

    return res


def run(lines: list) -> tuple:
    names_1 = ["A", "K", "Q", "J", "T"] + [str(i) for i in range(9, 1, -1)]
    names_2 = ["A", "K", "Q", "T"] + [str(i) for i in range(9, 1, -1)] + ["J"]

    res_1 = calc(parse(lines, names_1, 1), names_1)
    res_2 = calc(parse(lines, names_2, 2), names_2)

    assert res_1 == 250602641
    assert res_2 == 251037509

    return res_1, res_2
