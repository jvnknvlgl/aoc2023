import re

subs = {
    "one": "o1e",
    "two": "t2o",
    "three": "t3e",
    "four": "f4r",
    "five": "f5e",
    "six": "s6x",
    "seven": "s7n",
    "eight": "e8t",
    "nine": "n9e",
}


def calc(line: str) -> int:
    return int("".join([c for c in line if c.isdigit()][i] for i in [0, -1]))


def run(lines: list) -> tuple:
    res_1 = 0
    res_2 = 0

    for line in lines:
        res_1 += calc(line)

        for key in subs.keys():
            line = re.sub(key, subs[key], line)

        res_2 += calc(line)

    assert res_1 == 55834
    assert res_2 == 53221

    return res_1, res_2
