def parse(line: str) -> tuple:
    return tuple(
        [int(c) for c in line.split(":")[1].split("|")[i].split() if c] for i in [0, 1]
    )


def run(lines: list) -> tuple:
    res_1 = 0
    res_2 = 0

    match = []

    for line in lines:
        winner, actual = parse(line)

        count = sum([1 for card in actual if card in set(winner)])
        match.append(count)

        res_1 += 1 << count - 1 if count else 0

    res = [0] * len(match)

    for i in range(len(match)):
        res[i] = 1 + sum(res[i - match[-i - 1] : i])
        res_2 += res[i]

    assert res_1 == 20829
    assert res_2 == 12648035

    return res_1, res_2
