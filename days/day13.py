from itertools import groupby


def parse(lines: list) -> list:
    return [list(line) for c, line in groupby(lines, key=bool) if c]


def calc(valley: list, dist: int) -> int:
    for i in range(1, len(valley)):
        if (
            sum(
                sum(a != b for a, b in zip(l, r))
                for l, r in zip(reversed(valley[:i]), valley[i:])
            )
            == dist
        ):
            return i

    return 0


def run(lines: list) -> tuple:
    res_1 = 0
    res_2 = 0

    for valley in parse(lines):
        res_1 += calc(list(zip(*valley)), 0) + 100 * calc(valley, 0)
        res_2 += calc(list(zip(*valley)), 1) + 100 * calc(valley, 1)

    assert res_1 == 34772
    assert res_2 == 35554

    return res_1, res_2
