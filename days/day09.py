from itertools import pairwise


def parse(line: str) -> list:
    diffs = [list(map(int, line.split()))]

    while any(diffs[-1]):
        diffs.append([b - a for a, b in pairwise(diffs[-1])])

    return diffs[::-1]


def calc(diffs: list, ass: int) -> int:
    if ass == 2:
        diffs = [diff[::-1] for diff in diffs]

    diffs[0].append(0)

    for i, (a, b) in enumerate(pairwise(diffs)):
        diffs[i + 1].append(b[-1] - a[-1] if ass == 2 else a[-1] + b[-1])

    return diffs[-1][-1]


def run(lines: list) -> tuple:
    res_1 = 0
    res_2 = 0

    for line in lines:
        diffs = parse(line)

        res_1 += calc(diffs, 1)
        res_2 += calc(diffs, 2)

    assert res_1 == 1882395907
    assert res_2 == 1005

    return res_1, res_2
