def parse_1(lines: list) -> tuple:
    res = []

    for line in lines:
        res.append([int(c) for c in line.split(":")[1].split() if c])

    return tuple(res)


def parse_2(lines: list) -> tuple:
    res = []

    for line in lines:
        res.append(int("".join([c for c in line.split(":")[1] if c != " "])))

    return tuple(res)


def calc_1(time: int, rcrd: int) -> int:
    dist_prev = 0
    count = 0

    for press in range(1, time):
        dist = (time - press) * press

        if dist <= dist_prev and dist <= rcrd:
            return count
        elif dist > rcrd:
            count += 1

        dist_prev = dist

    return count


def calc_2(time: int, rcrd: int) -> int:
    return int(((time**2) - (4 * rcrd)) ** 0.5) + 1


def run(lines: list) -> tuple:
    res_1 = 1

    times, rcrds = parse_1(lines)

    for time, rcrd in zip(times, rcrds):
        res_1 *= calc_1(time, rcrd)

    time, rcrd = parse_2(lines)
    res_2 = calc_2(time, rcrd)

    assert res_1 == 211904
    assert res_2 == 43364472

    return res_1, res_2
