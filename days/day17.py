from heapq import heappop, heappush


def parse(lines: list) -> list:
    return [list(map(int, line)) for line in lines]


def calc(maze: list, step_min: int, step_max: int) -> int:
    todo: list = []
    seen = set()

    heappush(todo, (0, (0, 0, 0)))
    heappush(todo, (0, (0, 0, 1)))

    while len(todo) > 0:
        cost, (x, y, dir) = heappop(todo)

        if (x, y) == (len(maze) - 1, len(maze[0]) - 1):
            break

        if (x, y, dir) in seen:
            continue

        seen.add((x, y, dir))

        cost_prev = cost

        for s in [-1, 1]:
            cost = cost_prev
            x_next, y_next = x, y

            for i in range(1, step_max + 1):
                if dir == 1:
                    y_next = y + i * s
                else:
                    x_next = x + i * s

                if (
                    x_next < 0
                    or x_next > len(maze) - 1
                    or y_next < 0
                    or y_next > len(maze[0]) - 1
                ):
                    break

                cost += maze[x_next][y_next]

                if i >= step_min:
                    heappush(todo, (cost, (x_next, y_next, 1 - dir)))

    return cost


def run(lines: list) -> tuple:
    maze = parse(lines)

    res_1 = calc(maze, 1, 3)
    res_2 = calc(maze, 4, 10)

    assert res_1 == 742
    assert res_2 == 918

    return res_1, res_2
