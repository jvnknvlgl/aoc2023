def parse(lines: list, ass: int) -> tuple:
    hashes = []
    seqs = lines[0].split(",")

    for seq in seqs:
        hash = 0

        if ass == 2:
            label = seq[: len(seq) - (1 if "-" in seq else 2)]
        else:
            label = seq

        for c in label:
            hash = ((hash + ord(c)) * 17) % 256

        hashes.append(hash)

    return hashes, seqs


def calc_1(hashes: list) -> int:
    return sum(hashes)


def calc_2(hashes: list, seqs: list) -> int:
    res = 0

    boxes: list[list] = [[] for _ in range(256)]
    focal: list[list] = [[] for _ in range(256)]

    for hash, seq in zip(hashes, seqs):
        label = seq[: len(seq) - (1 if "-" in seq else 2)]

        if "-" in seq:
            if label in boxes[hash]:
                index = boxes[hash].index(label)

                boxes[hash].pop(index)
                focal[hash].pop(index)
        elif "=" in seq:
            if label in boxes[hash]:
                index = boxes[hash].index(label)

                boxes[hash][index] = label
                focal[hash][index] = int(seq[-1])
            else:
                boxes[hash].append(label)
                focal[hash].append(int(seq[-1]))

    for i, box in enumerate(focal):
        for j, slot in enumerate(box):
            res += (i + 1) * (j + 1) * slot

    return res


def run(lines: list) -> tuple:
    hashes, _ = parse(lines, 1)
    res_1 = calc_1(hashes)

    hashes, seqs = parse(lines, 2)
    res_2 = calc_2(hashes, seqs)

    assert res_1 == 505459
    assert res_2 == 228508

    return res_1, res_2
