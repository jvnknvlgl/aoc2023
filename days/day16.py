import sys

dirs = {
    (".", 1, 0): [(1, 0)],
    (".", -1, 0): [(-1, 0)],
    (".", 0, 1): [(0, 1)],
    (".", 0, -1): [(0, -1)],
    ("|", 1, 0): [(1, 0)],
    ("|", -1, 0): [(-1, 0)],
    ("|", 0, 1): [(1, 0), (-1, 0)],
    ("|", 0, -1): [(1, 0), (-1, 0)],
    ("-", 1, 0): [(0, 1), (0, -1)],
    ("-", -1, 0): [(0, 1), (0, -1)],
    ("-", 0, 1): [(0, 1)],
    ("-", 0, -1): [(0, -1)],
    ("/", 1, 0): [(0, -1)],
    ("/", -1, 0): [(0, 1)],
    ("/", 0, 1): [(-1, 0)],
    ("/", 0, -1): [(1, 0)],
    ("\\", 1, 0): [(0, 1)],
    ("\\", -1, 0): [(0, -1)],
    ("\\", 0, 1): [(1, 0)],
    ("\\", 0, -1): [(-1, 0)],
}


def parse(lines: list) -> list:
    return list(map(list, lines))


def solve(maze: list, beam: list, path: set, pos: tuple) -> list:
    if pos in path:
        return beam

    path.add(pos)

    i, j, i_next, j_next = pos

    i += i_next
    j += j_next

    if i > len(maze) - 1 or j > len(maze) - 1 or i < 0 or j < 0:
        return beam

    beam[i][j] = "#"

    for i_next, j_next in dirs[(maze[i][j], i_next, j_next)]:
        beam = solve(maze, beam, path, (i, j, i_next, j_next))

    return beam


def calc(maze: list) -> tuple:
    res = []

    for i in range(len(maze)):
        for init in [
            (i, -1, 0, 1),
            (i, len(maze), 0, -1),
            (-1, i, 1, 0),
            (len(maze), i, -1, 0),
        ]:
            beam = [["." for _ in range(len(maze))] for _ in range(len(maze))]
            beam = solve(maze, beam, set(), init)

            res.append(sum([line.count("#") for line in beam]))

    res_1 = res[0]
    res_2 = max(res)

    return res_1, res_2


def run(lines: list) -> tuple:
    maze = parse(lines)

    depth = sys.getrecursionlimit()
    sys.setrecursionlimit(int(1e4))

    res_1, res_2 = calc(maze)

    sys.setrecursionlimit(depth)

    assert res_1 == 6622
    assert res_2 == 7130

    return res_1, res_2
