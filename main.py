import argparse
import glob
import sys
import time


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-l",
        "--last",
        action="store_true",
        help="only run the last available day",
    )

    parser.add_argument(
        "-s",
        "--skip",
        nargs="+",
        help="skip running the specified days",
    )

    args = parser.parse_args()

    sys.path.append("days")
    days = [day.split(".")[0].split("/")[1] for day in glob.glob("days/day*")]
    days.sort()

    if args.last:
        days = [days[-1]]

    try:
        skip = list(map(int, args.skip))
    except TypeError:
        skip = []

    times = 0

    for i, day in enumerate(map(__import__, days)):
        if (i == 0 or i in skip) and not args.last:
            continue

        with open(f"data/{days[i]}.txt") as f:
            lines = f.read().splitlines()

        begin = time.time()

        res_1, res_2 = day.run(lines)

        total = (time.time() - begin) * 1000
        times += total

        print(f"\n-- {days[i]} --")
        print(f"res_1: {res_1}")
        print(f"res_2: {res_2}")
        print(f"time: {total:.3f} ms")

    print("\n-- total --")
    print(f"time: {times:.3f} ms\n")


if __name__ == "__main__":
    main()
