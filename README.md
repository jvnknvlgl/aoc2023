# AOC 2023

Solutions to [Advent of Code](https://adventofcode.com/) 2023. Solutions are in `days/`, every time a new solution is added it will automatically be executed when calling `python main.py` (except for the template day, `day00.py`). If you only want to run the last solution, there is a `--last` flag available. This project uses [Poetry](https://python-poetry.org/) to handle dependencies.

